import itertools
import pickle
import time
import numpy as np
import sklearn.pipeline
import sklearn.preprocessing
from os import system
from joblib import dump, load
from collections import defaultdict
from abc import ABC, abstractmethod
from sklearn.linear_model import SGDRegressor
from sklearn.kernel_approximation import RBFSampler
from futeamigos.environment.robot_tag_v2 import RobotTagV2
from futeamigos.environment.grids import *
from futeamigos.environment import DeterministicFollowerAgent
from futeamigos.environment.character import CharacterObservation
from futeamigos.environment.directions import CardinalDirections
from futeamigos.environment.world import Character, World


class Agent(ABC):
    """
    An abstract class representing an agent
    """

    @abstractmethod
    def get_action(self, observation: CharacterObservation) -> int:
        """
        Return an action given an observation

        Args:
            observation: the observation to which the agent should react
        """

    @abstractmethod
    def get_value(self, observation: CharacterObservation, action: int) \
            -> float:
        """
        Get the value of a given observation

        Args:
            observation: the world state being observed by the agent
        """


class Approximator:
    def __init__(self, env, alpha, width, height, num_actions):
        observation_examples = np.array([np.clip(env.observation_space.sample(),
            -10, 10) for x in range(10000)])
        self.env = env
        self.scaler = sklearn.preprocessing.StandardScaler()
        self.scaler.fit(observation_examples)

        self.featurizer = sklearn.pipeline.FeatureUnion([
                ("rbf1", RBFSampler(gamma=5.0, n_components=100)),
                ("rbf2", RBFSampler(gamma=2.0, n_components=100)),
                ("rbf3", RBFSampler(gamma=1.0, n_components=100)),
                ("rbf4", RBFSampler(gamma=0.5, n_components=100))
        ])
        self.featurizer.fit(self.scaler.transform(observation_examples))

        self.models = []
        self.width = width
        self.height = height
        for i in range(num_actions):
            model = SGDRegressor(alpha=alpha, learning_rate="constant")
            model.partial_fit([self.featurize(env.reset())], [0])
            self.models.append(model)


    def featurize(self, s):
        """
        """
        features = [s[0], s[1], s[2], s[3]]
        # print("state", state)
        scaled = self.scaler.transform([features])
        featurized = self.featurizer.transform(scaled)
        return featurized[0]


    def predict(self, s, a=None):
        """
        """
        features = self.featurize(s)
        if a is None:
            return np.array([m.predict([features])[0] for m in self.models])
        else:
            return self.models[a].predict([features])[0]


    def update(self, s, a, y):
        """
        """
        features = self.featurize(s)
        self.models[a].partial_fit([features], [y])


class SarsaLFAAgent:

    def __init__(self, env, alpha, epsilon, gamma, width, height, num_actions):
        self.approximator = Approximator(env, alpha, width, height, num_actions)
        self.num_actions = num_actions
        self.epsilon = epsilon
        self.gamma = gamma


    def epsilon_greedy_policy(self, observation: CharacterObservation) -> np.array:
        """
            Returns all action probabilities based on epsilon-greedy exploration
            algorithm.
        """
        m = self.num_actions
        A = np.ones(m, dtype=float) * self.epsilon/m
        q_values = [self.approximator.predict(observation, action) for action in range(m)]
        best_action =  np.argwhere(q_values == np.amax(q_values))

        if(len(best_action) > 1):
            best_action = np.random.choice(np.array(best_action).squeeze()) #break tie
        A[best_action] += (1.0 - self.epsilon)
        return A


    def get_action(self, observation: CharacterObservation, deterministic=False) -> int:
        """
            Returns a CardinalDirection (int) indicating where the agent should
            move to in order to tag the enemy.
        """
        choice = -1
        if(not deterministic):
            next_action_probs = self.epsilon_greedy_policy(observation)
            choice = np.random.choice(np.arange(len(next_action_probs)), p=next_action_probs)
        else:
            q_values = [self.approximator.predict(observation, action) for action in range(9)]
            choice = np.argmax(q_values)
        next_action = CardinalDirections.DESLOC_TO_VECTOR[choice]
        return CardinalDirections.VECTOR_TO_DESLOC[next_action]


    def get_value(self, observation: CharacterObservation, action: int) -> float:
        """
        """
        return self.approximator.predict(observation, action)


    def update(self, state, action, reward, next_state, next_action):
        """
        """
        #print('state ', state)
        #print('action ', action)
        #print('reward ', reward)
        #print('next state ', next_state)
        #print('next action ', next_action)
        Q = self.approximator.predict(next_state)
        target = reward + self.gamma*Q[next_action]
        self.approximator.update(state, action, target) #update weights


def evaluate(env, my_agent, enemy, n_steps):
    reward_history = []
    for sub_ep in range(n_steps):
        observation = env.reset()
        done = False
        steps = 0
        rewards = []

        while steps < 100 and not done:
            #system('clear')
            #print('Evaluating ')
            #env.render()
            #time.sleep(0.01)
            action = my_agent.get_action(
                observation=observation,
                deterministic=True)

            observation, reward, done, _ = env.step(actions=(action,
                enemy.get_action(observation=observation)))
            steps += 1
            rewards.append(reward)
        reward_history.append(sum(rewards))
    return reward_history


def my_reward(world: World) -> float:
    if(world.player_1.touched(world.player_2)):
        return -15
    return 1

TEST_GRID = [
    "W             W  ",
    "    W  W   W     ",
    "       W WWW  W W",
    "WW  W        WW  ",
    "    WW WWW      W",
    "WW         WWWW  ",
]

def train(max_steps=100, max_episodes=30000):
    print('training...')
    rewards = [None]
    env = RobotTagV2.from_grid(grid=TEST_GRID)
    height = len(TEST_GRID)
    width = len(TEST_GRID[0])

    env.reward_function = my_reward #lambda world: 1
    my_agent = SarsaLFAAgent(env, alpha=0.01, epsilon=0.3, gamma=0.9, width=width,
        height=height, num_actions=9)

    follower_agent = DeterministicFollowerAgent(
        world = env.world,
        me = env.world.player_2,
        enemy = env.world.player_1
    )

    for episode in range(max_episodes):
        observation = env.reset()
        #print('observation ', observation)
        action = my_agent.get_action(observation)
        state = observation
        done = False
        ep_reward = 0
        n_steps = 0

        if(episode % 100 == 0):
            #print('Evaluation')
            reward_history = evaluate(env, my_agent, follower_agent, 20)
            #with open('../../output/rewards_eval_lfa.csv', 'a') as csv_file:
            #    csv_file.write(f"{sum(reward_history)/20}\n")
            #print('Eval rewards:', reward_history, sum(reward_history)/20)
            rewards.append(sum(reward_history)/20)

        while not done:
            #system('clear')
            #print('Episode {}/{} Last Reward {}'.format(episode, max_episodes, rewards[-1]))
            #env.render()
            enemy_action = follower_agent.get_action(observation)
            observation, reward, done, world = env.step(actions=(action, enemy_action))
            next_state = observation
            next_action = my_agent.get_action(next_state)
            my_agent.update(state, action, reward, next_state, next_action)

            if(n_steps == max_steps):
                break

            ep_reward += reward
            n_steps+=1
            state = next_state
            action = next_action

    return rewards[1:]


if __name__== '__main__':
    start = time.time()
    rewards = train(100, 10000)
    print(rewards)
    print('training took ', time.time()-start, 's')

    #dump(model, 'model.joblib')
    #np.save('../../output/rewards_lfa.npy', np.array(rewards))

    #import matplotlib.pyplot as plt
    #rewards = np.load('../output/rewards.npy', allow_pickle=True)[1:]
    #plt.plot(range(len(rewards)), rewards)
    #plt.show()

    #Q_test = np.load('../../output/state_action_values_lfa.npy', allow_pickle=True).item()
    #test(env=RobotTagEnv.from_grid(grid=EMPTY_GRID), Q=Q_test, max_episodes=100)
