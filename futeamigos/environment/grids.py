"""
This module defines basic grids for simple experiments
"""

BASIC_GRID = [
    "W   W    W    W  ",
    "  WWW  W   W  W  ",
    "    W  W WWW  W W",
    "WW  W     W  WW  ",
    "    WW WWWW     W",
    "WW        WWWWW  ",
]

ULTIMATE_GRID = [
    "W             W  ",
    "    W  W   W     ",
    "       W WWW  W W",
    "WW  W        WW  ",
    "    WW WWW      W",
    "WW         WWWW  ",
]
