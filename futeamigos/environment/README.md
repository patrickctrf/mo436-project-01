# Futeamigos Env

This Env is implemented using Gym interface, so it may work with a plethora of Reinforcement Learning libraries


## TL;DR

To see if a dummy agent that stand still while being followd by an A* follower, execute:

```
python futeamigos/environment/robot_tag.py
```

## Game rules

There are two players in a grid of NxM. The first player is defined by the letter A, whereas player 2 is the letter B.
Ws are walls:

```
|     W     |
| A   W     |
|           |
|     W   B |
```

Players can only move one cell at each step.  They cannot walk through an Wall or leave the grid. All directions are allowed. If both players are one cell apart, then the game ends

```
|     W     |
| AB  W     |
|           |
|     W     |
```

The **reward** at each step is the euclidean distance between the players. If you are controling the scaping player, it may be a good idea to use the negative of the reward.

The observation is the players position along with the last action executed by them

```
Frame 0
|     W     |
| A   W     |
|           |
|     W   B |

Observation: (
    1, # A.x
    1, # A.y
    0, # A last action was to stand still
    9, # B.x
    3, # B.y
    0  # B last action was to stand still
)
              

Frame 1

|     W     |
|  A  W     |
|         B |
|     W     |

Observation: (
    2, # A.x
    1, # A.y
    3, # A last action was to move EAST
    8, # B.x
    3, # B.y
    5  # B last action was to move SOUTH
)
```

Always `futeamigos.environment.directions.CardinalDirections` to map from Cardinal Directions to integers when given actions an agent! **Do not use hardcoded integers!**

## Training my algorithm

Import stuff

```Python
from futeamigos.environment import RobotTagEnv
from futeamigos.environment import BASIC_GRID
from futeamigos.environment import DeterministicFollowerAgent
```

Here RobotTagEnv is the environment and BASIC_GRID defines a basic scene for training.
Defining a new scenes is easy, just follow the pattern in `futeamigos.environment.grid`

DeterministicFollowerAgent is an A* agent, that will follow the other player

```Python
env = RobotTagEnv.from_grid(grid=BASIC_GRID)

follower_agent = DeterministicFollowerAgent(
    world=env.world,
    me=env.world.player_2,
    enemy=env.world.player_1
)

my_agent = MyAgent()
```

Here we instantiate the env with a predefined grid.
We then create the A* agent informing about the world, which player the A* is and which player it should follow. It is not a typical practice to pass the `env` as an arg to the agent as we do here.

Normally, as defined by the OpenAI Gym interface, the agents should receive information about the env only through `env.step`, but we need the A* to have access to the walls positions.

`my_agent` is the agent created by the user. Again: it should **not** receive the `env` as an arg as we did with A*. Use the initialization to set hyperparams, such as `learning rate` and etc.

What follows is a basic reinforcement learning loop

```Python
for episode in range(10):
    observation = env.reset()
    done = False

    while not done:
        # Clear and print the grid in stdout
        system('clear')
        env.render()

        # get_action should return an int
        my_action = my_agent.get_action(
            observation=observation
        )

        enemy_action = follower_agent.get_action(observation)

        observaton, reward, done, _ = env.step(
            actions=(
                my_action,
                enemy_action
            )
        )

```

We are executing 10 episodes, and each episode begins with an `env.reset` that will position the players in a random position (we take care to not initialize then too close) and returns the very first `observation`.

While the env do not returns `done=True`, we render the env, then take an action from our agent, take an action from the follower agent and execute `env.step`, which receives a Tuple with `actions=(player 1 action player 2 action)`.

`env.step` returns the next `observation`, `reward` and `done`, as defined in the last section.

It is yout job to modify this loop as you need in order to train your algorithm. You probabily will need to inform your agent about the reward and the episode end somehow. It is up to you to define the new interfaces that will receive these information, and the interface that will execute a learning step.

But never, **never**, **NEVER** modify the `env` interfaces or `agent.get_action` interface.

## Custom reward functions

If you want to use custom reward functions, do as follows

```Python
def custom_function(world: World) -> float:
    # world arg controls the player and walls position
    # it has every information you need about the grid current state

    print(world.player_1.x, world.player_1.y)
    print(world.player_2.x, world.player_2.y)

    for wall in world.walls:
        print(wall.x, wall.y)
    
    # Do some calculation and return
    return 0

env.reward_function = custom_function
```