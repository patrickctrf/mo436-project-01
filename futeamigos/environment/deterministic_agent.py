from abc import ABC, abstractmethod
from typing import Any

from futeamigos.environment.character import CharacterObservation
from futeamigos.environment.directions import CardinalDirections
from futeamigos.environment.world import Character, World
from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder

CHAR_TO_WEIGHT = {
    ' ': 1,
    'A': 1,
    'B': 1,
    'W': 0
}


class Agent(ABC):
    """
    An abstract class representing an agent
    """

    @abstractmethod
    def get_action(self, observation: CharacterObservation,
                   deterministic: bool = False) -> int:
        """
        Return an action given an observation

        Args:
            observation: the observation to which the agent should react
            deterministic: if True, return the best action for the given
                observation
        """

    @abstractmethod
    def get_value(self, observation: CharacterObservation, action: int) \
            -> float:
        """
        Get the value of a given observation

        Args:
            observation: the world state being observed by the agent
        """


class DeterministicFollowerAgent(Agent):
    """
    An Agent that follows an enemy using A* algorithm

    Args:
        world: the world from which the agent will take the representation
        me: the Character representing the agent in the world
        enemy: the Character representing the enemy in the world
    """

    def __init__(self, world: World, me: Character, enemy: Character):
        self.world = world

        self.me = me
        self.enemy = enemy

    def get_action(self, observation: CharacterObservation,
                   deterministic: bool = True) -> int:
        """
        Returns a CardinalDirection (int) indicating where the agent should
        move to in order to tag the enemy
        """
        grid = world_to_grid(world=self.world)

        start = grid.node(
            x=self.me.x,
            y=self.me.y
        )

        end = grid.node(
            x=self.enemy.x,
            y=self.enemy.y
        )

        finder = AStarFinder(diagonal_movement=DiagonalMovement.always)
        path, _ = finder.find_path(start, end, grid)

        if not path:
            return CardinalDirections.CENTER

        next_step = (
            path[1][0] - self.me.x,
            path[1][1] - self.me.y
        )

        return CardinalDirections.VECTOR_TO_DESLOC[next_step]

    def get_value(self, observation: CharacterObservation, action: int) \
            -> float:
        """
        Does nothing
        """
        return 0


def world_to_grid(world: World) -> Grid:
    """
    Encode the World grid into a representation understandable by pathfiding
    lib

    Args:
        world: the world for which the grid will be encoded

    Returns:
        The grid representation
    """
    grid = world.render_grid()

    matrix = []
    for line in grid:
        matrix.append(
            list(map(lambda char: CHAR_TO_WEIGHT[char], line))
        )

    return Grid(matrix=matrix)
