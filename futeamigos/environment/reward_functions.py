import numpy as np
from futeamigos.environment.world import World


def euclidean_distance(world: World) -> float:
    """
    Returns the euclidean distance between the players

    Args:
        world: the grid world
    """
    return np.linalg.norm(
        np.array(world.player_1.position()) -
        np.array(world.player_2.position())
    )
