from typing import List, Tuple

from futeamigos.environment import RobotTagEnv
from futeamigos.environment.character import CharacterObservation
from futeamigos.environment.world import World

SimplifiedObservation = Tuple[int, int, int, int]


class RobotTagV2(RobotTagEnv):
    """
    Gym Environment for the RobotTag (futeamigos) task
    This version does not observe the players last actions

    To build an Env from a predefined grid, use `from_grid` constructor.

    Args:
        world: an World instance defining the grid
    """

    def observe(self) -> SimplifiedObservation:
        """
        Returns a simplified version of the observation
        """
        return (
            *self.world.player_1.position(),
            *self.world.player_2.position()
        )

    def step(self, *args, **kwargs) \
            -> Tuple[SimplifiedObservation, float, bool, str]:
        """
        Makes a step in the env, moving both characters

        Args:
            actions: a tuple of two integers, each one defining the direction
                to each character will move

        Returns:
            A tuple with both characters observation concatenated, the reward,
                the done signal and an info signal
        """
        _, reward, done, info = super().step(
            *args, **kwargs
        )

        observation = self.observe()

        return observation, reward, done, info

    def reset(self, *args, **kwargs) -> SimplifiedObservation:
        """
        Reset the env state to a random state
        """
        super().reset(*args, **kwargs)

        return self.observe()

    @classmethod
    def from_grid(cls,  grid: List[List[str]]) -> 'RobotTagV2':
        """
        Creates the env from a predefined grid. See the module
        `futeamigos.grids` for examples

        Args:
            grid: the grid, a List of strings containing `W` characters to
                demark the walls
        """
        world = World.from_grid(grid=grid)

        return RobotTagV2(world=world)
