from collections import namedtuple
from typing import List, Tuple

from futeamigos.environment.world import World

Position = namedtuple('Position', ['x', 'y'])
WorldFrame = namedtuple(
    'WorldFrame', ['player_1_position', 'player_2_position', 'walls_position',
                   'player_1_last_direction', 'player_2_last_direction'])


def take_world_snapshot(world: World) -> WorldFrame:
    """
    """

    return WorldFrame(
        player_1_position=world.player_1.position(),
        player_2_position=world.player_2.position(),
        player_1_last_direction=world.player_1.last_direction,
        player_2_last_direction=world.player_2.last_direction,
        walls_position=[
            Position(x=wall.x, y=wall.y)
            for wall in world.walls
        ]
    )
