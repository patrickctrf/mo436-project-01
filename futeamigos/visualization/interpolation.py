from collections import namedtuple
from typing import Iterable, Tuple

import pygame
from futeamigos.visualization.drawables import (CELL_SIZE, Character, Grid,
                                                Player1, Player2, Wall)
from futeamigos.visualization.frame import WorldFrame

SPEED = 1


class LinearInterpolation:
    def __init__(self, from_value: int, to_value: int):
        """
        """
        self.from_value = from_value
        self.to_value = to_value

        if self.from_value > self.to_value:
            self.speed = -SPEED
        else:
            self.speed = SPEED

    def __iter__(self):
        """
        """
        self.current_value = self.from_value

        return self

    def __next__(self):
        """
        """
        if self.from_value == self.to_value:
            raise StopIteration

        if self.current_value + self.speed != self.to_value:
            self.current_value += self.speed

            return self.current_value

        raise StopIteration


class CharacterPositionInterpolation:
    """
    """

    def __init__(self, x: LinearInterpolation, y: LinearInterpolation):
        self.x = iter(x)
        self.y = iter(y)

    def final_position(self):
        return self.x.to_value, self.y.to_value

    def __iter__(self):
        """
        """
        self.x_stop_iter = False
        self.y_stop_iter = False

        return self

    def __next__(self):
        """
        """

        if not self.x_stop_iter or not self.y_stop_iter:
            try:
                x = next(self.x)

            except StopIteration:
                self.x_stop_iter = True
                x = self.x.to_value

            try:
                y = next(self.y)

            except StopIteration:
                self.y_stop_iter = True
                y = self.y.to_value

            return x, y

        raise StopIteration


def player_position_interpolation(
    init_position: Tuple[int, int],
    final_position: Tuple[int, int],
    player_class: Character) \
        -> Iterable[CharacterPositionInterpolation]:
    """
    """
    from_x, from_y = player_class.from_grid_to_screen_position(
        grid_x=init_position[0],
        grid_y=init_position[1]
    )

    to_x, to_y = player_class.from_grid_to_screen_position(
        grid_x=final_position[0],
        grid_y=final_position[1]
    )

    interpolation = CharacterPositionInterpolation(
        x=LinearInterpolation(
            from_value=from_x,
            to_value=to_x
        ),
        y=LinearInterpolation(
            from_value=from_y,
            to_value=to_y
        ),
    )

    return iter(interpolation)


class InterpolateFrames:
    def __init__(self, frame_a: WorldFrame, frame_b: WorldFrame,
                 grid_height: int, grid_width: int):
        """
        """
        self.grid_height = grid_height
        self.grid_width = grid_width

        self.player_1_interpolation = player_position_interpolation(
            init_position=frame_a.player_1_position,
            final_position=frame_b.player_1_position,
            player_class=Player1
        )

        self.player_2_interpolation = player_position_interpolation(
            init_position=frame_a.player_2_position,
            final_position=frame_b.player_2_position,
            player_class=Player2
        )

        self.wall_positions = frame_b.walls_position

    def __iter__(self) -> pygame.Surface:
        """
        """
        self.p1_stop_iteration = False
        self.p2_stop_iteration = False

        return self

    def __next__(self):
        """
        """
        if not self.p1_stop_iteration or not self.p2_stop_iteration:
            surface = pygame.Surface(
                size=(
                    CELL_SIZE * self.grid_width,
                    CELL_SIZE * self.grid_height
                )
            )

            Grid.draw(
                grid_width=self.grid_width,
                grid_height=self.grid_height,
                screen=surface
            )

            for wall in self.wall_positions:
                Wall.draw(
                    grid_x=wall.x,
                    grid_y=wall.y,
                    screen=surface
                )

            try:
                x, y = next(self.player_1_interpolation)

            except StopIteration:
                x, y = self.player_1_interpolation.final_position()
                self.p1_stop_iteration = True

            Player1.draw(x=x, y=y, screen=surface)

            try:
                x, y = next(self.player_2_interpolation)

            except StopIteration:
                x, y = self.player_2_interpolation.final_position()

                self.p2_stop_iteration = True

            Player2.draw(x=x, y=y, screen=surface)

            return surface

        raise StopIteration
