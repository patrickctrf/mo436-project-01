import os
import random
import time
from collections import defaultdict
from typing import Dict, Tuple

import numpy as np
from futeamigos.environment import (CardinalDirections,
                                    DeterministicFollowerAgent)
from futeamigos.environment.robot_tag_v2 import RobotTagV2
from futeamigos.environment.world import World
from futeamigos.sam_qlearning.buffer import Buffer, Experience
from futeamigos.sam_qlearning.table import Table
from tqdm import tqdm

TEST_GRID = [
    "W             W  ",
    "    W  W   W     ",
    "       W WWW  W W",
    "WW  W        WW  ",
    "    WW WWW      W",
    "WW         WWWW  ",
]

DIRS = [
    'NW',
    'N',
    'NE',
    'E',
    'SE',
    'S',
    'SW',
    'W',
    'C',
]

N_ZERO = 5


def observation_to_kwarg(observation: Tuple[int]) -> Dict[str, int]:
    """
    Helper function to make my lif easier
    """
    return dict(
        agent_x=observation[0],
        agent_y=observation[1],
        enemy_x=observation[2],
        enemy_y=observation[3],
    )


class QAgent:
    """
    Tabular QAgent for RobotTag env

    Args:
        height: the grid height
        width: the grid width
    """

    def __init__(self, height: int, width: int):
        self.table: Table = Table(
            width=width,
            height=height
        )

        self.state_visits = np.zeros(
            (
                width,
                height,
                width,
                height
            )
        )

        self.state_action_visits = np.zeros(
            (
                width,
                height,
                width,
                height,
                len(list(CardinalDirections())),
            )
        )

    def get_action(
            self,
            observation: Tuple[int],
            deterministic: bool = False) -> int:
        """
        Returns an action to the given observation

        Args:
            observation: the observed state
            epsilon: we sample a random number in (0, 1). If this number is
                smaller than epsilon, then we take a random action. Otherwise,
                we take the best action
            deterministic: if True, foces to return the best action
        """
        epsilon = N_ZERO/(N_ZERO + self.state_visits[observation])
        self.state_visits[observation] += 1

        if random.random() > epsilon:
            deterministic = True

        if deterministic:
            action = self.table.get_max_action(
                **observation_to_kwarg(observation=observation)
            )

        else:
            action = random.choice(list(CardinalDirections()))

        self.state_action_visits[(*observation, action)] += 1

        return action

    def get_value(self, observation: Tuple[int], action: int):
        """
        Returns the value for a tuple observation/action

        Args:
            observation: the observation for which the value should be returned
            action: the action for which the value should be returned
        """
        return self.table.get_value(
            **observation_to_kwarg(observation=observation),
            action=action
        )

    def update_step(self, experience: Experience, gamma: float = 0.99):
        """
        Do a Q Learning update step

        Args:
            experience: a tuple containing an observation, the next observation
                an action and the reward for the transition
            gamma: the horizon discount
            learning_rate: the QLearning learning rate
        """
        obs, next_obs, action, reward = experience

        learning_rate = 1/self.state_action_visits[(*obs, action)]

        old_value = self.table.get_value(
            **observation_to_kwarg(observation=obs),
            action=action
        )

        future_value = self.table.get_max_value(
            **observation_to_kwarg(observation=next_obs)
        )

        new_value = old_value + learning_rate * (
            reward + gamma * future_value - old_value
        )

        self.table.set_value(
            **observation_to_kwarg(obs),
            action=action,
            value=new_value
        )


def alive_reward_function(world: World) -> float:
    """
    """
    if world.player_1.touched(other=world.player_2):
        return -15

    return 1


def train(episodes: int = int(30000), debug: bool = False) -> QAgent:
    env: RobotTagV2 = RobotTagV2.from_grid(grid=TEST_GRID)

    height = len(TEST_GRID)
    width = len(TEST_GRID[0])

    agent = QAgent(height=height, width=width)

    buffer = Buffer(max_size=int(1e6))

    bot = DeterministicFollowerAgent(
        world=env.world,
        me=env.world.player_2,
        enemy=env.world.player_1
    )

    env.reward_function = alive_reward_function

    all_rewards = []

    for episode in range(episodes):
        done = False
        observation = env.reset()
        steps = 0

        while not done and steps < 20:
            action = agent.get_action(observation=observation)

            next_observation, reward, done, _ = env.step(
                actions=(
                    action,
                    bot.get_action(observation=None)
                )
            )

            buffer.add_step(
                observation=observation,
                next_observation=next_observation,
                action=action,
                reward=reward
            )

            observation = next_observation

            steps += 1

        if episode > 1000:
            for _ in range(int(1e4)):
                experience = buffer.get_random_step()
                agent.update_step(experience=experience)

            reward_history = []
            if episode % 100 == 0:
                for sub_ep in range(20):
                    observation = env.reset()
                    done = False
                    steps = 0
                    rewards = []

                    while steps < 100 and not done:
                        if sub_ep == 900:
                            env.render()
                            time.sleep(0.01)

                            print(observation)

                            action_values = agent.table.get_all_values(
                                **observation_to_kwarg(observation=observation)
                            )

                            print(', '.join('{}: {:.2f}'.format(d, v) for d,
                                            v in zip(DIRS, action_values)))

                        action = agent.get_action(
                            observation=observation,
                            deterministic=True
                        )

                        observation, reward, done, _ = env.step(
                            actions=(
                                action,
                                bot.get_action(observation=[])
                            )
                        )

                        steps += 1

                        rewards.append(reward)

                    reward_history.append(sum(rewards))

                with open('tabular_ql_rewards.csv', 'a') as csv_file:
                    csv_file.write(f"{sum(reward_history)/20}\n")

                if debug:
                    print(
                        'Eval rewards:', 
                        reward_history, 
                        sum(reward_history)/20
                    )

                all_rewards.append(sum(reward_history)/20)

                agent.table.save()
    
    return all_rewards


if __name__ == '__main__':
    train()
