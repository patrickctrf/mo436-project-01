import pickle

from futeamigos.environment import DeterministicFollowerAgent, RobotTagEnv
from futeamigos.sam_qlearning.q_learning import TEST_GRID, QAgent
from futeamigos.sam_qlearning.table import Table
from futeamigos.visualization.visualization import VisualizeTrained
from futeamigos.environment.robot_tag_v2 import RobotTagV2

if __name__ == '__main__':
    env = RobotTagV2.from_grid(grid=TEST_GRID)

    agent = QAgent(
        height=len(TEST_GRID),
        width=len(TEST_GRID[0])
    )

    with open('table_file', 'rb') as table_file:
        agent.table = pickle.load(table_file)

    bot = DeterministicFollowerAgent(
        world=env.world,
        me=env.world.player_2,
        enemy=env.world.player_1
    )

    visualization = VisualizeTrained(
        player_1_agent=agent,
        player_2_agent=bot,
        env=env,
        player_1_valuemap=False
    )

    visualization.render(episodes=10)
