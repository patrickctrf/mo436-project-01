import random
from typing import Tuple

Experience = Tuple[Tuple[int], Tuple[int], int, float]


class Buffer:
    def __init__(self, max_size: int = 1000):
        self.max_size = max_size
        self.current_position = 0

        self.buffer = []

        for i in range(self.max_size):
            self.buffer.append((None, None, None, None))

    def add_step(self, observation: Tuple[int], action: Tuple[int],
                 next_observation: Tuple[int], reward: float):
        """
        """
        if self.current_position == self.max_size:
            self.current_position = 0

        self.buffer[self.current_position] = (
            observation,
            next_observation,
            action,
            reward
        )

        self.current_position += 1

    def get_random_step(self) -> Experience:
        """
        """
        step = random.randrange(start=0, stop=self.current_position)

        return self.buffer[step]
