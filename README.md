# mo436-project-01

Primeiro projeto da disciplina de Reinforcement Learning - MO436 do IC - Unicamp, com a professora Esther.

## Instalação

Use esse pacote através do pip!

```
cd repositorio
pip install -e .
```

Dessa forma você pode importar o módulo como segue:

```
import futeamigos
```

Como instalamos o pacote em modo de desenvolvimento com `-e`, você pode desenvolver dentro do repositório clonado sem precisar reinstalar o pacote a cada modificação.

## Como começar?

Dê uma lida em `futeamigos\environment\README.md`

## Quem é o Patrick?

O melhor pica-fio da Unicamp.